# Frequently asked questions

## Can you upgrade package X to version Y.Z ?

As mentioned in the introduction, `AlmaLinux` and `CentOS` are rebuilds made from Red Hat Entreprise Linux.
We therefore do not upgrade packages on our own.
If your package does not exist in the CERN offering, we could propose it upstream to EPEL, or include it in our internal CERN/CERNONLY repository.
You can contact us for further information.

## Why should I mirror my software repository on `linuxsoft` and not use `company X` repository ?

A random company may not exist in 5 years.
Many machines at CERN do not have and should not have full Internet access.
By requesting a mirror, you make sure your deployement will be consitent even if a repository disappears.

## The version of package X.Y-Z is vulnerable to a security issue.

Please note that Red Hat backports security fixes on older versions, so the package NAME-VERSION-RELEASE is not relevant.
A better way to check if a CVE has been fixed is to use `rpm -q --changelog <package>` and read/grep the content.

## Why not switching to ubuntu/arch/gentoo/alpine/mint/elementary/fedora/tails/openSUSE ?

Red Hat derived distributions are the official WLCG distributions and having a long lifecycle (> 10 years) allows Linux Support to provide a effective support to scientists for a full LHC run.
