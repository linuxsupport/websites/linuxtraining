# Mirror service on linuxsoft.cern.ch

## Mirrors type

We provide two types of mirrors:

 * Official mirrors: we are listed as official URL for a few projects such as [CentOS](https://www.centos.org/download/mirrors/) and [AlmaLinux](https://mirrors.almalinux.org/).
 * Local mirrors: we just mirror the content so it is available in case something goes wrong on external servers, it allows us to have a consistent and reproducible installation process.

You can request a local mirror, by creating a [SNOW](https://cern.service-now.com/service-portal?id=service_element&name=linux-desktop) request to Linux support.

Please note that being an official mirror for a new open source project needs to go through a careful review (space, bandwidth, rules, content validation, etc...).

## Content

You can browse local mirrors at : <http://linuxsoft.cern.ch/mirror/>
