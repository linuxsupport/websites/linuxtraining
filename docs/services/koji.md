# Linux software building service koji.cern.ch

## Introduction

Koji is an open source project used by Red Hat and Fedora to build RMPs in an automated way.

From [https://pagure.io/koji/](https://pagure.io/koji/):

> Koji's goal is to provide a flexible, secure, and reproducible way to build software.

## Key features

Build RPM from source rpms file or from git repository, mainly gitlab at CERN.

## Usage

Please read the [documentation](https://linux.web.cern.ch/koji/).

Accessing CERN Koji can also be done through this [link](https://koji.cern.ch/).

The recommended way to build RPMs using Koji and Gitlab's CI infrastructure is documented here:
<https://gitlab.cern.ch/linuxsupport/rpmci>

And it was presented in this [ASDF session](https://indico.cern.ch/event/884283/#17-rpmci-building-rpms-in-your)
