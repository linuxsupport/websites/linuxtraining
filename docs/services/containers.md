# Containers

Linux Support regularly updates container images, which are built using Koji. [Here](https://linux.web.cern.ch/dockerimages/) you can find CERN's docker images documentation.

## Docker hub

Official images can be downloaded from <https://hub.docker.com/u/cern/>

For more efficiency these are the precise links for each supported OS:

- [cc7](https://hub.docker.com/r/cern/cc7-base)
- [alma8](https://hub.docker.com/r/cern/alma8-base)
- [alma9](https://hub.docker.com/r/cern/alma9-base)

## CERN gitlab registry

A local copy in gitlab can also be used:

 * <https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry>
 * <https://gitlab.cern.ch/linuxsupport/alma8-base/container_registry>
 * <https://gitlab.cern.ch/linuxsupport/alma8-base/container_registry>
