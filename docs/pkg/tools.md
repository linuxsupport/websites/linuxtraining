# Tools

## yumdowloader

`yumdownloader` is part of the `yum-utils` package:

```
$ yum install yum-utils
```

### Download an rpm

```
$ yumdownloader wget
```

### Download the rpm source package

```
$ yumdownloader --source wget
```

## db_recover

`db_recover` is part of the `libdb-utils` package:

```
$ yum install libdb-utils
```

### Recover the /var/lib/rpm database

After a crash, recovery of the rpmdb may be needed:

```
$ /usr/bin/db_recover -h /var/lib/rpm
```

## repoquery

`repoquery` is part of yum-utils package:

```
$ yum install yum-utils
```

### Query a remote repository without installing it

```
$ repoquery --repofrompath=repotmp,https://linuxsoft.cern.ch/cern/slc5X/x86_64/yum/os/ --repoid=repotmp -qa
```
