# Red Hat Package Manager (RPM)

RPM is a free an open-source package management system. Check [Wikipedia](https://en.wikipedia.org/wiki/RPM_Package_Manager) for a longer description.

## Query

Query all installed packages on your system:

```
$ rpm -qa
```

You might want to grep for a certain string (e.g. "openstack" in this case) to get all related packages:

```
$ rpm -qa | grep openstack
python3-openstackclient-5.2.0-1.el8.noarch
python3-openstacksdk-0.46.1-1.el8.noarch
python-openstackclient-lang-5.2.0-1.el8.noarch
```

In order to show information regarding an installed package:

```
$ rpm -qi iptables
Name        : iptables
Version     : 1.4.21
Release     : 35.el7
Architecture: x86_64
Install Date: Wed 03 May 2023 10:02:09 AM CEST
Group       : System Environment/Base
Size        : 1556976
License     : GPLv2
Signature   : RSA/SHA256, Wed 14 Oct 2020 08:51:02 PM CEST, Key ID 24c6a8a7f4a80eb5
Source RPM  : iptables-1.4.21-35.el7.src.rpm
Build Date  : Thu 01 Oct 2020 06:52:54 PM CEST
Build Host  : x86-01.bsys.centos.org
Relocations : (not relocatable)
Packager    : CentOS BuildSystem <http://bugs.centos.org>
Vendor      : CentOS
URL         : http://www.netfilter.org/
Summary     : Tools for managing Linux kernel packet filtering capabilities
Description :
The iptables utility controls the network packet filtering code in the
Linux kernel. If you need to set up firewalls and/or IP masquerading,
you should install this package.
```

Check installed kernel versions:
```
$ rpm -q kernel
kernel-5.14.0-284.11.1.el9_2.x86_64
```

Check if a file is part of a package:
```
$ rpm -qf /etc/sysconfig/
filesystem-3.16-2.el9.x86_64
pcp-6.0.1-4.el9.x86_64
```

## Verify

Verifying a package compares information about the installed files in the package with information about the files taken from the package metadata stored in the rpm database.

```
$ rpm -V iptables
..?......  c /etc/sysconfig/ip6tables-config
..?......  c /etc/sysconfig/iptables-config

Help:
    S file Size differs
    M Mode differs (includes permissions and file type)
    5 digest (formerly MD5 sum) differs
    D Device major/minor number mismatch
    L readLink(2) path mismatch
    U User ownership differs
    G Group ownership differs
    T mTime differs
    P caPabilities differ
```

This might be useful, for instance, if you want to check whether you have modified some of the default files installed along a package.

## Install a single package

```
$ rpm -i myrpm.rpm
```

## Remove a single package

```
$ rpm -e myrpm
```
