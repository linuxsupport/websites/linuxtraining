# Yellowdog Updater Modified (YUM)

YUM, similarly to RPM, is a command-line package-management utility. It is different, and better, in that it allows for automatic updates and package dependency management. Yum works with software repositories that allow for the updates, yet runs RPM underneath.

## YUM Basics

### Cleaning the  yum cache

YUM uses a cache to speed-up its operations, it is always a good idea to "clean" whenever you first log in to a machine:

```
$ yum clean all
```

### Retrieve information about your packages

In order to query packages available on your system:
```
$ yum list
```

Check installed kernel versions:
```
$ yum list kernel
Last metadata expiration check: 0:00:14 ago on Wed 05 Jul 2023 11:22:57 AM CEST.
Installed Packages
kernel.x86_64                                          5.14.0-284.11.1.el9_2                                           @anaconda
```

Check if a file is part of an available package:
```
$ yum provides /usr/bin/korgac
Loaded plugins: changelog, kernel-module, ovl, post-transaction-actions,
              : priorities, product-id, search-disabled-repos, tsflags,
              : versionlock
              
[...]

7:kdepim-4.10.5-7.el7.x86_64 : KDE PIM (Personal Information Manager)
                             : applications
Repo        : updates
Matched from:
Filename    : /usr/bin/korgac
```

### Install a package

```
$ yum install wget
```

### Remove a package

```
$ yum remove wget
```

### Update all the installed packages in your system

```
$ yum update
```

## YUM repositories

The above commands are very similar to the ones shown in [RPM](./rpm.md). However, the true power of yum is managing packages through repositories.

A repository is a storage location where packages are stored, and thus can be retrieved and installed.

Examples of the repositories are the [CC7 CERN repository](https://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/Packages/) and the [ALMA9 repository](http://linuxsoft.cern.ch/cern/alma/9/CERN/x86_64/Packages/), where one can find all CERN-related packages maintained by us.

Although the recommended way of configuring software repositories is using puppet (through the [CERN central configuration management service](https://gitlab.cern.ch/ai/it-puppet-module-osrepos/blob/master/code/README.md)) one might need to perform certain operations by hand:

Check the list of enabled repositories:
```
$ yum repolist
```

This will show the enabled repositories that YUM will be aware of in that system. If trying to install a certain package, and it is not found, you might want to add additional repositories where that package might be stored. CentOS default repositories are very minimal as it is a system meant for servers, so for instance not many GUI packages are in the default repositories.

YUM repositories are just files under `/etc/yum.repos.d` (if your system is not puppet managed), with a similar format:

Firstly, check where the repository is located:
```
$ yum list | grep cernbox
puppet-cernbox.noarch                                                                    2.5-1.el9.cern                           @locmap
```
Then you can use the following command to show the information:
```
$ cat /etc/yum.repos.d/locmap.repo
[locmap]
name=locmap [stable]
baseurl=http://linuxsoft.cern.ch/internal/repos/potd9-stable/$basearch/os
enabled=1
gpgcheck=True
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
```

You can also check all configured repositories in your system (even if not enabled) running:

```
$ yum repolist all
```

And to enable a repository, you can do it either:

For the command you are running (temporarily):
```
$ yum --enablerepo=docker-ce-test/x86_64 install docker
```

Or permanently editing the corresponding `.repo` file, changing the `enabled=` param to `1`:

```
$ cat /etc/yum.repos.d/docker-ce.repo
[docker-ce-test]
[...]
enabled=1
[...]
```
