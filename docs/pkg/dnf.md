# DNF / Dandified YUM

## Introduction

DNF or Dandified YUM is the next generation version of YUM (Yellowdog Updater, Modified), a package manager for .rpm based systems. It is available in any OS distribution higher or equal to the version 8 (so for RHEL8, ALMA8 and CentOS8).

DNF has a more robust architecture compared to YUM, leading to a better performance, memory footprint, and an additional consistency in the UI. It's designed to resolve package dependencies and install packages into systems.

One key advantage of DNF is its ability to perform package management tasks in a consistent and safe way. It's designed to be extensible, allowing plugins to extend its functionality. As a result, DNF can handle package installation, removal, upgrades, and more in a streamlined and efficient way.

## Relationship with YUM

Even though DNF is a distinct package management system, it maintains broad compatibility with its predecessor. Most YUM commands can be replicated with DNF.

For a list of commands and their functions, please refer to the [YUM page](https://linux-training.web.cern.ch/pkg/yum/). In most cases, simply replacing 'yum' with 'dnf' in the command will yield the expected results.

## Conclusion

DNF is a powerful and efficient package management system. Its increased consistency, reliability, and extensibility make it an excellent tool for managing software on .rpm based systems.

For further details and instructions on using DNF, consider checking the man pages by typing `man dnf` in the terminal, or visiting the [official DNF documentation](https://dnf.readthedocs.io/en/latest/).
