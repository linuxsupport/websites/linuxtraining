# Prerequisites

 * You have a CERN account
 * You know linux basic commands `ls`, `cp`, `mv`, `scp`, etc...
 * You know how to connect to a remote machine with `ssh` and/or Putty.

## Suscribe to services before the training

Because some services need a few hours for synchronisation/replication we ask users to follow these steps a few days before the training.

A reminder was sent to you by the training staff.

 * Login to <https://cern.ch/account>
 * Click on "Resources and Services" on the right column
 * Click on "List Services" icon
 * If not done please subscribe to:
    * "LXPLUS and Linux"
    * "Cloud infrastructure"
    * "EOS/CERNBox"
