# Checking you permissions

## Try to login on lxplus with your account

    $ ssh yourlogin@lxplus
    Last login: Thu Jun 29 15:23:22 2023 from XXX
    * ********************************************************************
    * Welcome to lxplus7111.cern.ch, CentOS Linux release 7.9.2009 (Core)
    * Archive of news is available in /etc/motd-archive
    * Reminder: you have agreed to the CERN
    *   computing rules, in particular OC5. CERN implements
    *   the measures necessary to ensure compliance.
    *   https://cern.ch/ComputingRules
    * Puppet environment: qa, Roger state: production
    * Foreman hostgroup: lxplus/nodes/login
    * Availability zone: cern-geneva-b
    * LXPLUS Public Login Service - http://lxplusdoc.web.cern.ch/
    * An AlmaLinux8 based lxplus8.cern.ch is now available
    * An AlmaLinux9 based lxplus9.cern.ch is now available
    * Please read LXPLUS Privacy Notice in http://cern.ch/go/TpV7
    * ********************************************************************
    [yourlogin@lxplus727 ~]$

## Try to login to the Openstack dashboard

Open <https://openstack.cern.ch>

## Try to login to the CERNBOX dashboard

Open <https://cernbox.cern.ch>

If you can access these three resources without any issue, you are good to go!
