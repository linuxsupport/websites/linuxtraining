# Systemd

This section contains a few one liners to help with everyday `systemd` operations

# systemctl

Enable a service (making it start at boot time)
```
$ systemctl enable nginx.service
```

Disable a service
```
$ systemctl disable nginx.service
```

Start a service
```
$ systemctl start nginx.service
```

Stop a service
```
$ systemctl stop nginx.service
```

Restart a service
```
$ systemctl restart nginx.service
```

Reload a service
```
$ systemctl reload nginx.service
```

Status of a service
```
$ systemctl status nginx.service 
```

List all of the units that systemd has loaded or attempted to load into memory
```
$ systemctl list-units --all # --list-unit-files list everything
```

# journalctl

Make journald persistent
```
$ mkdir -p /var/log/journal
```

```
$ cat /etc/systemd/journald.conf

[...]
[Journal]
Storage=persistent # set Storage to persistent
```

Display the timestamps in UTC
```
$ journalctl --utc
```

View log since last boot
```
$ journalctl -b
```

View log since a specified date
```
$ journalctl --since "2018-09-10 20:00" # --until can be used too
```

Follow logs (à la `tail -f`)
```
$ journalctl -f
```

Check a service log
```
$ journalctl -u crond
```

Check entries that involve an executable
```
$ journalctl /usr/bin/bash
```
