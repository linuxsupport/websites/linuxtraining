# Docker

For more documentation, please visit https://linux.web.cern.ch/dockerimages/

CERN currently builds and maintains docker images for the following suppported distributions:

| Distribution | Dockerhub URL |
|--------------|---------------|
| cc7   | [https://hub.docker.com/r/cern/cc7-base](https://hub.docker.com/r/cern/cc7-base)   |
| alma8 | [https://hub.docker.com/r/cern/alma8-base](https://hub.docker.com/r/cern/alma8-base) |
| alma9 | [https://hub.docker.com/r/cern/alma9-base](https://hub.docker.com/r/cern/alma9-base) |

## Installation on CC7

```
$ yum install docker-latest
$ systemctl enable docker-latest # Enable at boot time
$ systemctl start docker-latest
```

## Usage with CERN image

Run a container with bash:
```
$ docker run -it cern/cc7-base bash
```

Run and detach a container
```
$ docker run -it -d cern/cc7-base bash
29ce8fd9a414e43793b9b1c5989c623a19aacb5a052a3823aed5b5548a6d5a7f
```

Re-attach to your container
```
$ docker attach 29ce8fd9a414e43793b9b1c5989c623a19aacb5a052a3823aed5b5548a6d5a7f # ctrl p + ctrl q
```

## Check logs

```
$ docker logs 29ce8fd9a414e43793b9b1c5989c623a19aacb5a052a3823aed5b5548a6d5a7f # ctrl p + ctrl q
```

## Check running containers

```
$ docker ps
```
