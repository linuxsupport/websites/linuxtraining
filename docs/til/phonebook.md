# Phonebook

Similar to what can be found in the <https://phonebook.cern.ch/>, one can access it through the CLI.

## Usage
```
$ /usr/bin/phonebook --help
phonebook [--help]
              phonebook SEARCHSTRING [--all]

              phonebook [SEARCHSTRING] [--surname SN] [--firstname FN]
              [--building BD] [--office FL-OFF] [--email M@A]
              [--phone PH] [--mobile MB]
              [--department DP] [--group GP] [--section ST ] [--pobox PB]
              [--login LN] [--ccid ID] [--uid UID] [--gid GID]
              [--externals] [--yellowpages] [--all|--terse TTT] [--orderby OOO]
```

## Check account status

```
$ /usr/bin/phonebook -l linux --all
#------------------------------------------------------------------------------
Surname:            service
Firstname:          linux
Display Name:       linux service
E-mail:             linux.service@cern.ch
Telephone:          -                                         (internal:      -)
Mobile:             -                                         (internal:      -)
Facsimile:          -                                         (internal:      -)
Department:         Other
Group:              -
Section:            -
POBox:              -                                         (internal mail)
Bld. Floor-Room:    -
Organization:       -
Computer Center ID: 423567
Low user ID:        -

Computer account(s):
Login    Grp St Uid   Gid  Last login    Shell    Home directory

linux    def-cg sA 44767 2766 11/11/18 01:35 /bin/bash /
#-------------------------------------------------------------------------------
#Account St(atus): P(rimary), S(econdary), s(ervice), U(nknown)
#                  A(ctive),  D(isabled),  P(assword expired),  L(ocked out)
```
