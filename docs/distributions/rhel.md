# Red Hat Enterprise Linux

## Notice

!!! danger "End of Site License"
    Our site license ends on **May 31, 2029**, before the currently-scheduled start of Run 4. After this date, there is no guarantee that we will have another license that will allow you to continue to use Red Hat Enterprise Linux. It is not recommended to use RHEL for any usecase where you will not be able to reinstall completely (migration-in-place will not be supported) and migrate to another operating system before this date.

>All three OSs (RHEL7, RHEL8 and RHEL9) are provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository.

## Red Hat Enterprise Linux 7

### A bit of history

RHEL7 support limit date is the 30.06.2024.

## Red Hat Enterprise Linux 8

### A bit of history

RHEL8 was released in november of 2022, and the support limit date is the 31.05.2029.

### Repositories

All the information for the repositories can be found [here](https://linux.web.cern.ch/updates/rhel8/).

## Red Hat Enterprise Linux 9

### A bit of history

RHEL9 was released in december of 2022, and the support limit date is the 31.05.2032.

### Repositories

All the information for the repositories can be found [here](https://linux.web.cern.ch/updates/rhel9/).
