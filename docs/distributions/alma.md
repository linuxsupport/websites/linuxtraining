# AlmaLinux

## Alma8 and Alma9 history

Both Alma8 and Alma9 were made available at CERN on 16.01.2023, however they don't have the same end of support date. Alma8's support will end on 31.05.2029 and Alma9's support will end on 31.05.2032

>An Open Source, community owned and governed, forever-free enterprise Linux distribution, focused on long-term stability, providing a robust production-grade platform. AlmaLinux OS is ABI compatible with RHEL®.

## Repositories

All the information for the repositories can be found [here for Alma8](https://linux.web.cern.ch/updates/alma8/) and [here for Alma9](https://linux.web.cern.ch/updates/alma9/).

## Special Interest Groups (SIGs)

You can find more information accessing the AlmaLinux SIGs on [wiki](https://wiki.almalinux.org/sigs/).
