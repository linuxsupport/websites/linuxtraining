# CERN Linux Distributions

At CERN, six distributions are supported:

* CERN CentOS 7
* AlmaLinux 8
* AlmaLinux 9
* Red Had Enterprise Linux 7
* Red Had Enterprise Linux 8
* Red Had Enterprise Linux 9

AlmaLinux and CentOS are derived from Red Hat Enterprise Linux (RHEL), with Red Hat's trademarks removed, thus making it freely available.
AlmaLinux and CentOS developers use Red Hat's source code to create a final product similar to RHEL.

The distribution content is provided by Red Hat, no update to any existing package is provided by CERN.

If a bug is discovered, a report is sent to Red Hat who will propose a fix.

Technical support is primarily provided, at CERN, by Linux Support. Outside, support is provided by the community via official mailing lists, web forums, and chat rooms.

Participating in these open source initiatives is available to any individual. New contributors from CERN are always a great addition.

New releases are typically produced a few weeks after each Red Hat release.

In the following sections, details for each distribution will be given.
