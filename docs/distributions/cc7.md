# CERN CentOS 7

## A bit of history

CERN CentOS 7 was released in 2014 and will be supported until July 2024.

> CERN Community ENTerprise Operating System 7 is the upstream CentOS 7, built to integrate into the CERN computing environment but it is not a site-specific product: all CERN site customisations are optional and can be deactivated for external users.

In 2015, CERN began migrating away from the Scientific Linux collaboration to provide the next version (RHEL 7 rebuild).

## Default repositories

Each Red Hat based distribution is shipped with a few default repositories.

At CERN you can list them with:

    $ ls -l /etc/yum.repos.d/*.repo
    -rw-r--r--. 1 root root 1964 Nov 27  2018 /etc/yum.repos.d/CentOS-Base.repo
    -rw-r--r--. 1 root root 2395 Nov 27  2018 /etc/yum.repos.d/CentOS-CERN.repo
    -rw-r--r--. 1 root root 1008 Nov 27  2018 /etc/yum.repos.d/CentOS-CR.repo
    -rw-r--r--. 1 root root  331 Nov 27  2018 /etc/yum.repos.d/CentOS-Debuginfo.repo
    -rw-r--r--. 1 root root  210 Nov 27  2018 /etc/yum.repos.d/CentOS-fasttrack.repo
    -rw-r--r--. 1 root root 2105 Nov 27  2018 /etc/yum.repos.d/CentOS-Sources.repo
    -rw-r--r--. 1 root root 7983 Nov 27  2018 /etc/yum.repos.d/CentOS-Vault.repo
    -rw-r--r--. 1 root root 1008 Dec  7  2015 /etc/yum.repos.d/elrepo.repo
    -rw-r--r--. 1 root root  587 Sep  3  2014 /etc/yum.repos.d/epel.repo
    -rw-r--r--. 1 root root  665 Sep  3  2014 /etc/yum.repos.d/epel-testing.repo

These repositories are installed by `-release` rpm packages.

    $ rpm -qf /etc/yum.repos.d/epel.repo
    epel-release-7-2.1.el7.cern.noarch

Additional `-release` rpm packages can be searched with `yum search release`.

!!! Note ""
    Please note that on a puppet managed node the repositories are managed by the `osrepos` module, and repo files stored in an alternative directory `/etc/yum-puppet.repos.d/`.

### Details about the CERN specific repositories:

#### The `cernonly` repository

This is a special repository only available inside cern, it contains proprietary software and tools very cern specific that we cannot redistribute.

#### The `cern` repository

This repository contains all additional software that Linux Support adds to the main distribution. These packages are maintained at CERN.

## Special Interest Groups (SIGs)

A new concept is available in the CentOS community : the Special Interest Groups (SIGs).

From the [wiki](https://wiki.centos.org/SpecialInterestGroup) you can get a detailed description:

!!! Note ""
    Special Interest Groups (SIG) are smaller groups within the CentOS community that focus on a small set of issues, in order to either create awareness or to focus on development along a specific topic. Each SIG can seek a dedicated section on the wiki, along with an option to display the names of the people involved in that SIG. A mailing list on the CentOS list server can also be setup as well. As proper, and subject to available resources, other resources they might need (eg. a SCM system, web hosting space, admin services, etc.) may be offered.

For end users it results in additional packages available :

    $ yum search centos-release
    [...]
    centos-release.x86_64 : CentOS Linux release file
    centos-release-ansible26.noarch : Ansible 2.6 packages from the CentOS ConfigManagement SIG repository
    centos-release-azure.noarch : Yum configuration for CentOS Virt SIG Azure repo
    centos-release-ceph-hammer.noarch : Ceph Hammer packages from the CentOS Storage SIG repository
    centos-release-ceph-jewel.noarch : Ceph Jewel packages from the CentOS Storage SIG repository
    centos-release-ceph-luminous.noarch : Ceph Luminous packages from the CentOS Storage SIG repository
    centos-release-ceph-nautilus.noarch : Ceph Nautilus packages from the CentOS Storage SIG repository
    centos-release-configmanagement.noarch : Release file for ConfigManagement SIG repository
    centos-release-dotnet.noarch : Config to enable the repository for DotNet on CentOS
    centos-release-fdio.noarch : fd.io packages from the CentOS NFV SIG repository
    centos-release-gluster-legacy.noarch : Disable unmaintained Gluster repositories from the CentOS Storage SIG
    centos-release-gluster310.noarch : Gluster 3.10 (Long Term Stable) packages from the CentOS Storage SIG repository
    centos-release-gluster312.noarch : Gluster 3.12 (Long Term Stable) packages from the CentOS Storage SIG repository
    centos-release-gluster313.noarch : Gluster 3.13 (Short Term Stable) packages from the CentOS Storage SIG repository
    centos-release-gluster36.noarch : GlusterFS 3.6 packages from the CentOS Storage SIG repository
    centos-release-gluster37.noarch : GlusterFS 3.7 packages from the CentOS Storage SIG repository
    centos-release-gluster38.noarch : GlusterFS 3.8 packages from the CentOS Storage SIG repository
    centos-release-gluster39.noarch : Gluster 3.9 (Short Term Stable) packages from the CentOS Storage SIG repository
    centos-release-gluster40.x86_64 : Gluster 4.0 (Short Term Stable) packages from the CentOS Storage SIG repository
    centos-release-gluster41.x86_64 : Gluster 4.1 (Long Term Stable) packages from the CentOS Storage SIG repository
    centos-release-gluster41.noarch : Gluster 4.1 (Long Term Stable) packages from the CentOS Storage SIG repository
    centos-release-gluster5.noarch : Gluster 5 packages from the CentOS Storage SIG repository
    centos-release-gluster6.noarch : Gluster 6 packages from the CentOS Storage SIG repository
    centos-release-nfs-ganesha28.noarch : NFS-Ganesha 2.8 packages from the CentOS Storage SIG repository
    centos-release-nfv-common.noarch : Common release file to establish shared metadata for CentOS NFV SIG
    centos-release-openshift-origin.noarch : Common release file to establish shared metadata for CentOS PaaS SIG
    centos-release-openshift-origin13.noarch : Yum configuration for OpenShift Origin 1.3 packages
    centos-release-openshift-origin14.noarch : Yum configuration for OpenShift Origin 1.4 packages
    centos-release-openshift-origin15.noarch : Yum configuration for OpenShift Origin 1.5 packages
    centos-release-openshift-origin310.noarch : Yum configuration for OpenShift Origin 3.10 packages
    centos-release-openshift-origin311.noarch : Yum configuration for OpenShift Origin 3.11 packages
    centos-release-openshift-origin36.noarch : Yum configuration for OpenShift Origin 3.6 packages
    centos-release-openshift-origin37.noarch : Yum configuration for OpenShift Origin 3.7 packages
    centos-release-openshift-origin39.noarch : Yum configuration for OpenShift Origin 3.9 packages
    centos-release-openstack.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-kilo.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-liberty.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-mitaka.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-newton.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-ocata.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-pike.x86_64 : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-pike.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-queens.x86_64 : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-queens.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-rocky.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-openstack-stein.noarch : OpenStack from the CentOS Cloud SIG repo configs
    centos-release-opstools.noarch : Config to enable the repository for OpsTools SIG
    centos-release-ovirt35.noarch : oVirt 3.5 packages from the CentOS Virtualization SIG repository
    centos-release-ovirt36.noarch : oVirt 3.6 packages from the CentOS Virtualization SIG repository
    centos-release-ovirt40.noarch : oVirt 4.0 packages from the CentOS Virtualization SIG repository
    centos-release-ovirt41.noarch : oVirt 4.1 packages from the CentOS Virtualization SIG repository
    centos-release-ovirt42.noarch : oVirt 4.2 packages from the CentOS Virtualization SIG repository
    centos-release-ovirt43.noarch : oVirt 4.3 packages from the CentOS Virtualization SIG repository
    centos-release-paas-common.noarch : Common release file to establish shared metadata for CentOS PaaS SIG
    centos-release-qemu-ev.noarch : QEMU Enterprise Virtualization packages from the CentOS Virtualization SIG repository
    centos-release-scl.noarch : Software collections from the CentOS SCLo SIG
    centos-release-scl-rh.noarch : Software collections from the CentOS SCLo SIG (upstream scl only)
    centos-release-storage-common.noarch : Common release file to establish shared metadata for CentOS Storage SIG
    centos-release-virt-common.noarch : Common release file to establish shared metadata for CentOS Virt SIG
    centos-release-xen.x86_64 : CentOS Virt SIG Xen repo configs
    centos-release-xen-410.x86_64 : CentOS Virt Sig Xen repo configs for Xen 4.10
    centos-release-xen-412.x86_64 : CentOS Virt Sig Xen repo configs for Xen 4.12
    centos-release-xen-46.x86_64 : CentOS Virt Sig Xen repo configs for Xen 4.6
    centos-release-xen-48.x86_64 : CentOS Virt Sig Xen repo configs for Xen 4.8
    centos-release-xen-common.x86_64 : CentOS Virt Sig Xen support files
    centos-release-yum4.noarch : YUM4 packages from the CentOS ConfigManagement SIG repository

Please note that all these additional packages are provided as-is and support requests *should not* be addressed to the linux support team.

!!! Note ""
    Most of these repositories are available with the `osrepos` module in the puppet world.
