# Cernbox

Official documentation: <http://cernbox.web.cern.ch/>

## Introduction

CERNBox, CERN's proprietary cloud storage solution, offers comprehensive data storage to all CERN users, similar to platforms such as Google Drive and Dropbox. It allows for data storage, sharing, and synchronization across various devices, including smartphones, tablets, laptops, and desktops. Users can access their stored data through any web browser or file explorer, and it's entirely up to them to decide which data to share with other individuals or collaboration groups.

It is built on top of [EOS](eos.md)

## Installation

Although Cernbox is available through locmap, the recommended way is to follow the guide maintained by the CERNBox team: <https://cernbox.web.cern.ch/cernbox/downloads/linux/>
