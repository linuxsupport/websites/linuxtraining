# S3

Documentation: <https://clouddocs.web.cern.ch/object_store/s3cmd.html>

Support: <https://cern.service-now.com/service-portal?id=functional_element&name=S3ObjectStorage>

## Introduction

S3 is a storage API for various object storages.

Amazon S3, Ceph, Google Cloud, Azure etc... are supporting object storages.

CERN S3 implementation is based on Ceph.

Within the S3 storage, one stores objects (files/data) in a bucket (directory/place-holder).

To access S3 storage, an `access_key_id` and `secret_access_key` is needed.

## Install a client with yum

```
$ yum install s3cmd
```

## Configure s3cmd

```
$ s3cmd ls s3://lxtrn
```
