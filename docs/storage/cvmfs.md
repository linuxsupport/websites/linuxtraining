# CVMFS

Official documentation: <https://cvmfs.readthedocs.io/>

Support: <https://cernvm.cern.ch/portal/support>

## Introduction

The CernVM File System provides a scalable, reliable and low-maintenance software distribution service.

It was developed to assist High Energy Physics (HEP) collaborations to deploy software on the worldwide-distributed computing infrastructure used to run data processing applications.

CernVM-FS is implemented as a POSIX read-only file system in user space (a FUSE module).

Files and directories are hosted on standard web servers and mounted in the universal namespace `/cvmfs`.

## Install with locmap

```
$ locmap --enable cvmfs
$ locmap --configure cvmfs
```

## Access CVMFS

```
$ ls /cvmfs/
```
