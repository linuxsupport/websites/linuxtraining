# Andrew File System (AFS)

Official documentation: <https://cern.service-now.com/service-portal?id=kb_article&n=KB0002863>

Get support through [ServiceNow](https://cern.service-now.com/service-portal?id=service_element&name=afs-service)

## Introduction

The AFS (Andrew File System) Service provides networked file storage for CERN users, in particular home directories, work spaces and project spaces.

The AFS Service is based on OpenAFS, an open-source distributed filesystem which provides a client-server architecture for location-independent, scalable, and secure file sharing.

## Install with locmap

```
$ locmap --enable afs
$ locmap --configure afs
```

## Access AFS

```
$ ls /afs/cern.ch
```

All CERN registered users get an AFS home folder, that is indeed your `$HOME` folder in interactive systems such as [lxplus](https://lxplusdoc.web.cern.ch/).

An example for user `dabadaba`:

```
$ /afs/cern.ch/user/d/dabadaba/
```
