# EOS

EOS is a disk-based, low-latency storage service, developed at CERN, and in principle the successor of AFS.

Official documentation: <https://eos-web.web.cern.ch/eos-web/>

Support service: <https://cern.service-now.com/service-portal?id=service_element&name=eos-service>

## Introduction

The main target area for the service is physics data analysis, which is characterised by many concurrent users, a significant fraction of random data access and a large file-open rate.

## Install with locmap

```
$ locmap --enable eos
$ locmap --configure eos
```

## Access EOS

```
$ ls /eos/
```

As EOS is meant to replace AFS at some point, all CERN registered users get an EOS home. You can access this "home" folder in interactive systems such as [lxplus](https://lxplusdoc.web.cern.ch/), although it is not yet set to be the home folder.

An example for user `dabadaba`:

```
$ /eos/home-d/dabadaba/
```
