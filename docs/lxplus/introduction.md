# An introduction

Official documentation: <https://lxplusdoc.web.cern.ch/>

LXPLUS, the "Linux Public Login User Service" is the interactive logon service to Linux for all CERN users.
The cluster LXPLUS consists of public machines provided by the IT Department for interactive work.

You can use SSH to access it from anywhere in the world with your CERN account.

A list of all lxplus aliases can be found at <https://lxplusdoc.web.cern.ch/evolution/list/>

It will be used extensively during this course.

## Lxplus access

### From Linux

Most Linux distributions have an SSH client installed.

    $ ssh yourlogin@lxplus.cern.ch

### From Mac

Mac has a native SSH client included in the Terminal, so just open it and:

    $ ssh yourlogin@lxplus.cern.ch

### From Windows

You can use the [Putty](https://espace.cern.ch/winservices-help/NICEApplications/HelpForApplications/Pages/UsingPuttyCERN.aspx) tool.
