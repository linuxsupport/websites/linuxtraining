## Website

The Linux website is available at the following URL: <https://linux.cern.ch>

## CERN Documentation

The website contains all the end-user documentation, and we will extensively use it for the exercices during this tutorial.

It contains up-to-date information about the lifecycle of distributions at CERN and the future plans.

 * [CC7 (CERN CentOS 7)](https://linux.web.cern.ch/linux/centos7/)
 * [ALMA8 (AlmaLinux 8)](https://linux.web.cern.ch/almalinux/alma8intro/)
 * [ALMA9 (AlmaLinux 9)](https://linux.web.cern.ch/almalinux/alma9intro/)

[Weekly updates](https://linux.web.cern.ch/linux/) are documented with links to relevant Red Hat reports if available.

## Red Hat documentation

Given that both CentOS and AlmaLinux are derivatives of Red Hat Enterprise Linux (RHEL), the documentation pertaining to all three operating systems exhibits significant similarities. As a consequence, the RHEL documentation assumes particular relevance, serving as a valuable resource for users and administrators of these systems. It provides a wealth of information that's directly applicable to CentOS and AlmaLinux, given their shared lineage with RHEL.

Red Hat provides documentation for every released product :

 * [RHEL7 (Red Hat Enterprise Linux 7)](https://linux.web.cern.ch/rhel/rhel7/install/)
 * [RHEL8 (Red Hat Enterprise Linux 8)](https://linux.web.cern.ch/rhel/rhel8/install/)
 * [RHEL9 (Red Hat Enterprise Linux 9)](https://linux.web.cern.ch/rhel/rhel9/install/)

The Red Hat documentation is very useful to deep dive in specific parts of the system and is very well organised around topics (Installation, System administration, tuning, etc...)

## Linux support tools / open source tools

All published work is available under our Gitlab organisation: <https://gitlab.cern.ch/linuxsupport>. Do not hesitate to contribute.
