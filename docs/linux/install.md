# Installation at CERN

## Network database

To be able to install and use a system at CERN you need to first register it in the network database, also called LanDB:

Open <https://network.cern.ch>

Check if your machine is registered:

  * Click on 'View the registered information about a device'
  * Enter your device name or IP or MAC address
  * Check if the field 'Operating System' is set to 'Linux'

## Standard CC7 installation

Follow these [instructions](https://linux.web.cern.ch/centos7/docs/install/) in order to perform a CC7 installation from the network.

## Standard ALMA8 installation

[Here](https://linux.web.cern.ch/almalinux/alma8/install/) is an overview document of the installation. You will find a step by step guide as well.

## Standard ALMA9 installation

[Here](https://linux.web.cern.ch/almalinux/alma9/install/) is an overview document of the installation. You will find a step by step guide as well.

## Standard RHEL7 installation

[Here](https://linux.web.cern.ch/rhel/rhel7/install/) is an overview document of the installation.

## Standard RHEL8 installation

[Here](https://linux.web.cern.ch/rhel/rhel8/install/) is an overview document of the installation. You will find a step by step guide as well.

## Standard RHEL9 installation

[Here](https://linux.web.cern.ch/rhel/rhel9/install/) is an overview document of the installation. You will find a step by step guide as well.

## Kickstart installation

Next section will guide through aims2 and a unattended kickstart installation of a VM. This is a more advanced usecase and requires understanding of tools such as Anaconda Kickstart, PXE booting, etc.
