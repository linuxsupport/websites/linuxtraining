# Licenses

## Red Hat Entreprise Linux

!!! danger "End of Site License"
    Our site license ends on **May 31, 2029**, before the currently-scheduled start of Run 4. After this date, there is no guarantee that we will have another license that will allow you to continue to use Red Hat Enterprise Linux. It is not recommended to use RHEL for any usecase where you will not be able to reinstall completely (migration-in-place will not be supported) and migrate to another operating system before this date.

Documentation: <http://linux.web.cern.ch/linux/rhel/>

Since March 2022, CERN holds an academic site-license which allows for a certain usage of RHEL on the CERN site. This means that there is an unlimited number of licenses (which implies that there aren't any restrictions for its usage), any CERN employee can therefore create a RHEL virtual machine via Openstack.

Registered systems holding valid Red Hat Enterprise Linux license can be installed using the same methods as CC7.

Red Hat Entreprise Linux repositories are also mirrored internally and a `.repo` file needs to be copied after the installation.
