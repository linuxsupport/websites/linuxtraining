# Configuring a CERN machine

## Introduction

A tool called `locmap` (Local Configuration with Masterless Puppet) helps to configure your machine with Linux support puppet modules and is installed by default with the CERN supported distributions 
when 'Software Development Workstation (CERN Recommended Setup)' is select during the installation.

### Usage

#### Show information about your machine and associated users

```
$ locmap --info
--------------
Configuration:
--------------
    Configuration directory  : /etc/locmap
    Configuration file       : /etc/locmap/locmap.conf
    Puppet modules path      : /usr/share/puppet/modules
    Puppet config path       : /etc/puppetlabs/puppet/
    Puppet hiera path        : /etc/locmap/code/environments/
    Puppet environment       : production
    Log directory            : /var/log

---------
Hostname:
---------
architecture:'x86_64'
created:'20220629090813.0Z'
disallow_root:'False'
domain:'cern.ch'
fqdn:'itcdcli-nuc01.cern.ch'
hostname:'itcdcli-nuc01'
ip:'128.141.193.190'
ip6:'2001:1458:202:228::102:bb4d'
is_virtual:'False'
location:'0031 1-0003'
mainusers:'{'schwicke'}'
managedby:'set()'
manager:'set()'
modified:'20230622082423.0Z'
responsibles:'{'schwicke'}'
uptime:'0'
cern_domain:gpn

------
Users:
------
 schwicke	  IT-CD-CLI	     Primary	31	ulrich.schwickerath@cern.ch
```

#### List available modules:

Here you can see all the different modules you can have configured by locmap. For instance, `afs` and `eosclient` will configure the AFS and EOS filesystems so that you can access them from your machine:

* These are the available modules for both RHEL and Alma, with a version 8 or higher:
```
$ locmap --list
[Available Modules]
afs            [ enabled] 
cernbox        [ enabled] 
cernphone      [ enabled] 
chrony         [ enabled] 
cvmfs          [ enabled] 
eosclient      [ enabled] 
kerberos       [ enabled] 
lpadmin        [ enabled] 
nscd           [ enabled] 
postfix        [ enabled] 
ssh            [ enabled] 
sudo           [ enabled] 
zoom           [ enabled] 
```
* These are the available modules for CC7:
```
$ locmap --list
[Available Modules]
afs            [ enabled] 
cernbox        [ enabled] 
cvmfs          [ enabled] 
eosclient      [ enabled] 
kerberos       [ enabled] 
lpadmin        [ enabled] 
nscd           [ enabled] 
ntp            [ enabled] 
sendmail       [ enabled] 
ssh            [ enabled] 
sudo           [ enabled] 
zoom           [ enabled] 
```
You can notice some differences in the modules between locmap on 7 compared to 8 and 9. First of all, cernphone is a new module. Two modules have been replaced from the CC7 version to the versions 8 and 9. The first one is sendmail (CC7) with postfix (8 & 9) and the second one is ntp (CC7) with chrony (8 & 9).

#### Enable and configure a module

```
$ locmap --enable afs
[INFO] module afs enabled.
```

Once enabled, we are able to actually configure AFS in our machine:
```
$ locmap --configure afs
[INFO    ] Added user: schwicke
[INFO    ] Please wait while your system is being configured... 
[INFO    ] The run succeeded, and some resources were changed.
```

You can check it by listing the contents of your personal folder:

```
$ ls /afs/cern.ch/user/u/username/
```
(NOTE: Substitute the u for the initial of your username, and username for your actual username)

## AlmaLinux

### Alma8

You can find the information to install Locmap [here](https://linux.web.cern.ch/almalinux/alma8/locmap/).

### Alma9

You can find the information to install Locmap [here](https://linux.web.cern.ch/almalinux/alma9/locmap/).

## RHEL

You can find the information to install Locmap [here](https://linux.web.cern.ch/rhel/locmap/) if you are using either RHEL8 or RHEL9.
