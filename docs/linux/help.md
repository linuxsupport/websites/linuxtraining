# Getting help

## Service Status

Incidents and upcoming changes with the linux support service are reported using the IT tools.

You can suscribe to the [linux-users](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=159316) egroup to receive important updates by email.

The [SLS](https://cern.service-now.com/service-portal?id=service_availability) gives you a quick overview of the IT service status.

Upcoming changes and incidents are reported to the [IT Status Board](https://cern.service-now.com/service-portal?id=service_status_board&area=IT).

## Reporting problems

For reporting problems or asking for assistance, please contact the service desk at <https://cern.service-now.com/service-portal/>.
Checking the IT SSB and the IT Service Display will help to see if the problem is known.

For raising a ticket specifically for the Linux support team, you can use this [direct link](https://cern.service-now.com/service-portal?id=service_element&name=linux-desktop).

## Getting updates and help on IT projects

### IT Technical Users Meeting (ITUM)

This meeting provides a technical update on IT department services, including ‘heads-up’ information on future plans.
It is aimed at technical experts within CERN's Experiments and User Communities.

<http://cern.ch/itum>

Minutes and notes can be found in [Indico](https://indico.cern.ch/category/2958/).


### IT Consulting Service

The IT consulting service is a recent initiative from the IT Departement that offers:

  * The possibility to exchange ideas in order to understand the supported IT landscape at CERN.
  * Help with system architecture and design.
  * Assessment of impact on security, software licenses and cost.

You can make a request in [SNOW](https://cern.service-now.com/service-portal?id=service_element&name=it-consulting) for any enquiry.

Tell your managers!

### Online chat

For quick discussions a [Mattermost linux channel](https://mattermost.web.cern.ch/it-dep/channels/linux) exists.
Most IT projects have a channel.

Nonetheless, for support matters, it is always encouraged to create a SNOW ticket.
