# What is a kickstart ?

Kickstart is the default method to run unattended installations for Red Hat Entreprise linux and derivatives.

!!! note "From RHEL documentation:"
    Using kickstart, a system administrator can create a single file containing the answers to all the questions that would normally be asked during a typical installation.

Let's install an additional tool:

```
$ yum install pykickstart
```

After installing the package, you can validate a Kickstart file using the following command:

```
$ ksvalidator /path/to/kickstart.ks
```

The default CC7 [kickstart](http://linux.web.cern.ch/linux/centos7/docs/kickstart-example.ks):

```
$ cat /path/to/kickstart.ks
##############################################################################
#
# Example KickStart file for CC7 installations
#
# Important note: this file is intended as an example only, and users are
# expected to tailor it to their needs. In particular, users should:
#   - review the partition table
#   - set an encrypted root password
#
# To upload the Kickstart file to the AIMS installation service, run:
#
#     /usr/bin/aims2client addhost --hostname <hostname> \
#                          --kickstart kickstart-example.ks \
#                          --pxe --name cc7_x86_64
#
##############################################################################

# Text mode or graphical mode?
text
# Install or upgrade?
install
# System authorization information
auth --enableshadow --passalgo=sha512
# SElinux
selinux --enforcing
# Use network installation
url --url="http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/"

#
# If you want to enable updates at installation time (not recommended) uncomment following lines:
#
# repo --name="updates"  --baseurl http://linuxsoft.cern.ch/cern/centos/7/updates/x86_64
# repo --name="cern"     --baseurl http://linuxsoft.cern.ch/cern/centos/7/cern/x86_64
# repo --name="cernonly" --baseurl http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64
# repo --name="EPEL"     --baseurl http://linuxsoft.cern.ch/epel/7/x86_64
#

# Firewall configuration
firewall --enabled --port=7001:udp,4241:tcp --service=ssh
#
# Run the Setup Agent on first boot (the post section replaces firsboot
# for non-interactive installations
#
#firstboot --enable

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'

# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp  --ipv6=auto --activate

#
# Root passwordPLEASE REVIEW
# python -c 'import crypt,base64,os; print(crypt.crypt("password", "$6$" + base64.b64encode(os.urandom(6))))'
#
#rootpw --iscrypted *********************
rootpw --iscrypted $6$dADzhJp7$c9o9X4yPrK0yFAMMetP8Zgi4YzRAfudD98DEBqYR7BaY3Q/2TFsGZyp2ty02CYP6g9ynK2Brd8IjFIPTvqDGn0

# System timezone
timezone Europe/Zurich --isUtc --ntpservers=ip-time-2.cern.ch,ip-time-1.cern.ch

# X Window System configuration information
xconfig  --startxonboot

# System bootloader configuration: PLEASE REVIEW
bootloader --location=mbr --boot-drive=vda #--boot-drive=sda

# Partitioning information: PLEASE REVIEW
clearpart --all --initlabel --drives=vda
autopart --type=lvm

# Reboot after installation?
reboot

#
# You can list groups with "yum grouplist -v"
#
%packages
@additional-devel
@base
@cern-base
@cern-addons
@cern-addons-x11
@cern-openafs-client

%end

%post

#
# This section describes all the post-Anaconda steps to fine-tune the installation
#

# redirect the output to the log file
exec >/root/ks-post-anaconda.log 2>&1
# show the output on the 7th console
tail -f /root/ks-post-anaconda.log >/dev/tty7 &
# changing to VT 7 that we can see what's going on....
/usr/bin/chvt 7

#
# Set the correct time
#
/usr/sbin/ntpdate -bus ip-time-1 ip-time-2
/sbin/clock --systohc

#
# AIMS (CERN)
# Tell our installation server the installation is over.
# otherwise PXE installs will loop all-over-again
# If you are not using PXE install: just ignore this section
#
/usr/bin/curl -4 --max-time 20 --output /root/aims2-deregistration-ipv4.txt --silent \
    http://aims.cern.ch/aims/reboot || :

/usr/bin/curl -6 --max-time 20 --output /root/aims2-deregistration-ipv6.txt --silent \
    http://aims.cern.ch/aims/reboot || :

#
# Save the Kickstart file for future reference
#
# Note: this assumes that the Kickstart-file uploaded to AIMS is called <hostname>.ks
#

shost=`/bin/hostname -s`

/usr/bin/curl -4 --max-time 20 --output /root/${shost}-ipv4.ks --silent \
    http://aims.cern.ch/aims/ks || :

/usr/bin/curl -6 --max-time 20 --output /root/${shost}-ipv6.ks --silent \
    http://aims.cern.ch/aims/ks || :

#
# Configuration steps, based on
# http://cern.ch/linux/centos7/docs/install.shtml#manualpostinst
#  *** Please not that starting with CC 7.3 and newer releases `locmap` replaces
# old lcm tool ***
#
#/usr/sbin/lcm --configure --all

#
# These are the core modules enabled by default on a interactive install.
#
/usr/bin/locmap --enable afs
/usr/bin/locmap --enable kerberos
/usr/bin/locmap --enable lpadmin
/usr/bin/locmap --enable nscd
/usr/bin/locmap --enable ntp
/usr/bin/locmap --enable sendmail
/usr/bin/locmap --enable ssh
/usr/bin/locmap --enable sudo
#/usr/bin/locmap --enable eosclient
#/usr/bin/locmap --enable cvmfs
#

#
# Run locmap to apply CERN customisations
#
/usr/bin/host xldap.cern.ch
/usr/bin/sleep 2s
/usr/bin/locmap --configure all
#

# Configure automatic update system:
/usr/bin/systemctl enable yum-autoupdate

#
# Create accounts for LANdb-registered responsible and main users, give responsible
# root access, configure relevant printers
# Can not be added to %packages section due to dependencies from EPEL,install now
# (not needed for CC 7.3 and newest)
#
#/usr/bin/yum -y install cern-config-users
#/usr/sbin/cern-config-users --setup-all

# Done
exit 0

%end
```
