# Open a support case

## Information

Please have the following information available:

  * Hostname of the machine
  * Running kernel (you can obtain it running `uname -a`)
  * A specific problem description and its severity, that is, its impact on your operations
  * Additional relevant information about the affected systems: attach logs and error messages.
  * It is not unlikely that Linux Support will ask you to get access to the machine, in that case:

## Grant access to the support team

At CERN, a tool exists to give temporary access to your machine to the support staff.

```
$ yum install cern-linuxsupport-access
```

Enable access:

```
$ cern-linuxsupport-access enable
Enabled Kerberos access for CERN Linux.Support personnel.
Enabled SSH public key access for CERN Linux.Support personnel.
```

Disable access:

```
$ cern-linuxsupport-access disable
Disabled Kerberos access for CERN Linux.Support personnel.
Disabled SSH public key access for CERN Linux.Support personnel.
```
