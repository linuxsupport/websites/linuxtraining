# Linux Automated Installation Management Service (aims2client)

## Introduction

`aims2client` is the client-side software for communicating with the Linux Automated Installation Management Service (AIMS2). The client is designed to allow you to register and de-register hosts for PXE installation. You can use the client to register your Kickstart file, Anaconda/Kernel append options and the pxeboot target you wish to use for your installation.

The `aims2client` also allows you to interact with the pxeboot media library displaying information about already uploaded images or uploading your own pxeboot media.

## Quickstart aims2client

Please check the manpage (`man aims2client`) for advanced usage.

The [installation documentation](https://linux.web.cern.ch/installation/aims/) will guide you in details.

### List available images

```
$ aims2client showimg all
[...]
CC79_HP_G6_PLAIN_DRIVER       ,x86_64 ,Y   ,CC7.9 FOR HP 800 G6 WORKAROUND
CC79_X86_64                   ,x86_64 ,Y   ,CERN CENTOS 7.9 X86_64        
CC79_X86_64_E1000E            ,x86_64 ,Y   ,CERN CENTOS 7 X86_64 WITH KMOD-E1000E-3.8.4-3.EL7_9.ELREPO UPDATES FOR PROBLEMATIC NICS
CC7_X86_64                    ,x86_64 ,Y   ,CERN CENTOS 7 X86_64 (LATEST) 
[...]       
ALMA8_8.8_AARCH64             ,aarch64,Y   ,ALMALINUX 8 (20230522) AARCH64
ALMA8_8.8_X86_64              ,x86_64 ,Y   ,ALMALINUX 8 (20230522) X86_64 
ALMA8_AARCH64                 ,aarch64,Y   ,ALMALINUX 8 (20230621) AARCH64
ALMA8_X86_64                  ,x86_64 ,Y   ,ALMALINUX 8 (20230621) X86_64 
[...]
ALMA9_9.2_AARCH64             ,aarch64,Y   ,ALMALINUX 9 (20230511) AARCH64
ALMA9_9.2_X86_64              ,x86_64 ,Y   ,ALMALINUX 9 (20230511) X86_64 
ALMA9_AARCH64                 ,aarch64,Y   ,ALMALINUX 9 (20230621) AARCH64
ALMA9_X86_64                  ,x86_64 ,Y   ,ALMALINUX 9 (20230621) X86_64 
[...]
RHEL8_8.8_AARCH64             ,aarch64,Y   ,RED HAT ENTERPRISE LINUX 8 (20230519) AARCH64
RHEL8_8.8_X86_64              ,x86_64 ,Y   ,RED HAT ENTERPRISE LINUX 8 (20230519) X86_64
RHEL8_AARCH64                 ,aarch64,Y   ,RED HAT ENTERPRISE LINUX 8 (20230621) AARCH64
RHEL8_X86_64                  ,x86_64 ,Y   ,RED HAT ENTERPRISE LINUX 8 (20230621) X86_64
[...]
RHEL9_9.2_AARCH64             ,aarch64,Y   ,RED HAT ENTERPRISE LINUX 9 (20230510) AARCH64
RHEL9_9.2_X86_64              ,x86_64 ,Y   ,RED HAT ENTERPRISE LINUX 9 (20230510) X86_64
RHEL9_AARCH64                 ,aarch64,Y   ,RED HAT ENTERPRISE LINUX 9 (20230622) AARCH64
RHEL9_X86_64                  ,x86_64 ,Y   ,RED HAT ENTERPRISE LINUX 9 (20230622) X86_64
[...]
```

### View image details

```
$ aims2client showimg CC7_X86_64 --all
-------------------------------------------------------------------------------
Image Name:                CC7_X86_64
Architecture:              x86_64
UEFI:                      Y
Description:               CERN CENTOS 7 X86_64 (LATEST)
Boot options:              [none]
Admin e-groups:            none
Kernel source:             /mnt/data2/dist/cern/centos/7.9//os/x86_64/images/pxeboot/vmlinuz
Kernel size:               6769256 bytes
Kernel MD5 sum:            ce61bb2ecb36318c56d42c48db166fe3
Initial RAM disk source:   /mnt/data2/dist/cern/centos/7.9//os/x86_64/images/pxeboot/initrd.img
Initial RAM disk size:     55129656 bytes
Initial RAM disk MD5 sum:  f111c832ebac0a2f8a9fc99eda371205
Image Uploader:            dabadaba
Image uploaded at:         2020/11/16 00:00:00
-------------------------------------------------------------------------------
$ aims2client showimg ALMA8_X86_64 --all
-------------------------------------------------------------------------------
Image Name:                ALMA8_X86_64
Architecture:              x86_64
UEFI:                      Y
Description:               ALMALINUX 8 (20230621) X86_64
Boot options:              [none]
Admin e-groups:            lxsoft-admins@cern.ch
Kernel source:             8-snapshots/20230621/BaseOS/x86_64/os/images/pxeboot/vmlinuz
Kernel size:               10843912 bytes
Kernel MD5 sum:            a0c46885142726b2b13709725d36aba6
Initial RAM disk source:   8-snapshots/20230621/BaseOS/x86_64/os/images/pxeboot/initrd.img
Initial RAM disk size:     89063420 bytes
Initial RAM disk MD5 sum:  537d6bd416b91b71139c014fc63dc4b0
Image Uploader:            linuxci
Image uploaded at:         2023/06/28 08:33:37
-------------------------------------------------------------------------------
$ aims2client showimg ALMA9_X86_64 --all
-------------------------------------------------------------------------------
Image Name:                ALMA9_X86_64
Architecture:              x86_64
UEFI:                      Y
Description:               ALMALINUX 9 (20230621) X86_64
Boot options:              [none]
Admin e-groups:            lxsoft-admins@cern.ch
Kernel source:             9-snapshots/20230621/BaseOS/x86_64/os/images/pxeboot/vmlinuz
Kernel size:               12200632 bytes
Kernel MD5 sum:            e9defd6ec50266b2b9aee72bce4f13a5
Initial RAM disk source:   9-snapshots/20230621/BaseOS/x86_64/os/images/pxeboot/initrd.img
Initial RAM disk size:     100990072 bytes
Initial RAM disk MD5 sum:  9a78715075d5222155ddbbd2b20409a4
Image Uploader:            linuxci
Image uploaded at:         2023/06/28 08:24:40
-------------------------------------------------------------------------------
$ aims2client showimg RHEL8_X86_64 --all
-------------------------------------------------------------------------------
Image Name:                RHEL8_X86_64
Architecture:              x86_64
UEFI:                      Y
Description:               RED HAT ENTERPRISE LINUX 8 (20230621) X86_64
Boot options:              [none]
Admin e-groups:            lxsoft-admins@cern.ch
Kernel source:             8-snapshots/20230621/baseos/x86_64/os/images/pxeboot/vmlinuz
Kernel size:               10838352 bytes
Kernel MD5 sum:            c4da5c8b5bbfb7204698436b44c20855
Initial RAM disk source:   8-snapshots/20230621/baseos/x86_64/os/images/pxeboot/initrd.img
Initial RAM disk size:     89085008 bytes
Initial RAM disk MD5 sum:  7ad11975e1acb21b6a9e4d42d8b2b482
Image Uploader:            linuxci
Image uploaded at:         2023/06/28 13:11:52
-------------------------------------------------------------------------------
$ aims2client showimg RHEL9_X86_64 --all
-------------------------------------------------------------------------------
Image Name:                RHEL9_X86_64
Architecture:              x86_64
UEFI:                      Y
Description:               RED HAT ENTERPRISE LINUX 9 (20230622) X86_64
Boot options:              [none]
Admin e-groups:            lxsoft-admins@cern.ch
Kernel source:             9-snapshots/20230622/baseos/x86_64/os/images/pxeboot/vmlinuz
Kernel size:               12176920 bytes
Kernel MD5 sum:            905fff15acb468f1a53dcab465cf9f1c
Initial RAM disk source:   9-snapshots/20230622/baseos/x86_64/os/images/pxeboot/initrd.img
Initial RAM disk size:     100781240 bytes
Initial RAM disk MD5 sum:  29d05a5d9a21daaf13591a9f425844a7
Image Uploader:            linuxci
Image uploaded at:         2023/06/28 08:33:12
-------------------------------------------------------------------------------
```

### Add a host and install latest CC7

Say you want to configure kost `berries01` to boot automatically and be configured with a kickstart file `myfruit.ks`. You would achieve so by doing:

```
$ aims2 addhost berries01 --kickstart myfruit.ks
```

And setting the Operating System to `CC7_X86_64` as we saw this image (CERN CentOS 7)  was uploaded to AIMS with this name:
```
$ aims2 pxeon berries01 CC7_X86_64
```

Please note that your kickstart needs to contain a [snippet](http://linux.web.cern.ch/linux/centos7/docs/kickstart-example.ks) to deregister the host, so your installation will not loop.
