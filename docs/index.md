# Linux survival kit guide

Today, the CERN Linux Support team is responsible for over 50,000 devices (~35,000 in the Data Centre and ~15,000 across the HEP community and user devices).

Maintaining such a large number of devices and providing support is a challenge: hence the need for an end-user training is very important.
This training has been designed to give an overview of Linux services and also give users an overview of the different technologies provided by the IT Department.

Finally, users will get a better understanding about our choices around the supported distributions and how to request help for issues through the [SNow service portal](https://cern.service-now.com/service-portal?id=service_element&name=linux-desktop).

A number of links to more specific CERN trainings will be given when necessary.

## What this guide will not do

* This guide will not mention unsupported distributions. It will focus on the supported versions at CERN: i.e. _CERN CentOS 7, Alma and RHEL_.

* This guide is not an introduction to Linux. It is focused of CERN-related features (packages, repositories, filesystems, etc.). If you are new to Linux, you might want to check some of the courses available through the LMS course UDEMY for CERN, such as:

    For absolute beginners:

    * [Complete Linux Training Course to Get Your Dream IT Job 2023](https://www.udemy.com/course/complete-linux-training-course-to-get-your-dream-it-job/)

    For more experienced users:

    * [CentOS 7 Linux Server: Alternative to Red Hat Enterprise](https://cern.udemy.com/course/centos-7-linux-server-administration-alternative-to-red-hat-enterprise/)
    * Linux Academy Red Hat Certified Systems Administrator Prep ([SA1](https://cern.udemy.com/course/redhat-linux-administration-l/) and [SA2](https://cern.udemy.com/course/linux-administration-ll/))

* Please note that UDEMY licence is needed to take the courses. It can be found in LMS and has a cost.

* This guide will not explain how to manage machines or services in the datacentre, please check:
    * <https://cern.ch/clouddocs/>
    * <https://cern.ch/configtraining/>

* This guide will not teach Linux kernel internals. The idea is to give the user an overview of Linux usage at CERN.
To learn more about the Linux kernel and detailed software packages, please check the following links:
    * <https://access.redhat.com/documentation/en-us/>
    * <https://github.com/0xAX/linux-insides>


## Changelog and updates

* Version 3.0 (June 2023)
  General revision

* Version 2.0 (February 2020)
  General revision and migration to mkdocs

* Version 1.0 (November 2018)
  Initial version
