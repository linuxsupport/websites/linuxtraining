# Linux Survival Kit Guide

Here you can find the two websites that provide the information/documentation about the Linux Survival Kit Guide.

Available at LMS: https://lms.cern.ch/ekp/servlet/ekp?PX=N&TEACHREVIEW=N&PTX=&CID=EKP000043411&TX=FORMAT1&LANGUAGE_TAG=en&DECORATEPAGE=N

Or at https://linux-training.web.cern.ch/
